# MicroserviceConfigServer

http://localhost:9090/config/default

## Docker

> docker build --file=Dockerfile-configserver --tag=config-server:latest --rm=true .

> docker volume create --name=config-repo

> docker run --name=config-server --publish=9090:9090 --volume=config-repo:/var/lib/config-repo config-server:latest
